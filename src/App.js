import "./App.css";
import React, { useState } from "react";
import ReadFile from "./components/ReadFile";
import Output from "./components/Output";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

function App() {
	const [text, setText] = useState();
	const [link, setLink] = useState("");

	const showFile = async (e) => {
		const reader = new FileReader();
		reader.onload = async (e) => {
			const arrResult = [];
			let stringSplit;
			if (e.target.result) {
				stringSplit = e.target.result.split("\n");
				stringSplit.forEach((element) => {
					if (!element.includes("//") && !element.includes("*")) {
						arrResult.push(element);
					}
				});
				setText(arrResult.join(""));
			}
		};
		reader.readAsText(e.target.files[0]);
	};

	const switchLink = () => {
		if (link === "") setLink("/h");
		else setLink("");
	};

	return (
		<div className="App">
			<Router>
				<Switch>
					<Route exact path="/">
						<ReadFile showFile={showFile} />
					</Route>
					<Route exact path="/h">
						<Output text={text} />
					</Route>
				</Switch>
				<div className="page">
					<Link to={link} onClick={switchLink}>
						move page
					</Link>
				</div>
			</Router>
		</div>
	);
}

export default App;
