import React from "react";

const ReadFile = (props) => {
	return (
		<div>
			<h1>Choose text file</h1>
			<button>
				<label>
					<input type="file" onChange={props.showFile} hidden />
					Upload txt file
				</label>
			</button>
		</div>
	);
};

export default ReadFile;
