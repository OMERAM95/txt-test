import React from "react";

const Output = (props) => {
	return (
		<div>
			<h1>Your text output</h1>
			{props.text}
		</div>
	);
};

export default Output;
